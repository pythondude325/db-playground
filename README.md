# DB Playground
Running a real, fully featured database in the browser.

## Development

### Requirements
1. zig compiler v0.11.0
2. sqlite amalgamation unziped to sqlite/

### Dev Server
To start the development server `yarn start` in the `www/` directory.

Frontend code will automaticly be rebuilt when changed.

Zig code has to be rebuilt manually with zig build system. To do this, run
`zig build parcel` in the `zig/` directory.

## Possible new features
- Database reset button (closes the database and starts a new connection);
- Table list button (runs `select name from sqlite_schema;`)
- Import database from a .sqlite file (should be able to be done with https://www.sqlite.org/c3ref/deserialize.html)
- Export database to .sqlite file (can do with https://www.sqlite.org/c3ref/serialize.html)