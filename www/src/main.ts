/// <reference path="parcel.d.ts"/>
import sqlite_api from 'url:../../zig/zig-out/lib/sqlite_api.wasm';

import * as monaco from 'monaco-editor';

const encoder = new TextEncoder();
const decoder = new TextDecoder();

interface QuerySuccess {
    status: "success";
    columns: string[];
    results: string[][];
}

interface QueryFailure {
    status: "failure";
    test_res_code: number;
    error_message: string;
}

type QueryResult = QuerySuccess | QueryFailure;

function build_results_table(results: QueryResult): DocumentFragment {
    const fragment = new DocumentFragment();

    if ("test_res_code" in results) {
        const p = document.createElement("p");
        p.innerText = `Failure with error code ${results.test_res_code}`;
        fragment.appendChild(p);
        return fragment;
    }

    const result_count_label = document.createElement("p");
    result_count_label.textContent = `${results.results.length} results`;
    fragment.appendChild(result_count_label);

    const table = document.createElement("table");

    const thead = document.createElement("thead");
    const header_row = document.createElement("tr");
    for (const column_name of results.columns) {
        const th = document.createElement("th");
        th.textContent = column_name;
        header_row.appendChild(th);
    }
    thead.appendChild(header_row);
    table.appendChild(thead);

    const tbody = document.createElement("tbody");
    for (const row of results.results) {
        const tr = document.createElement("tr");
        for (const datum of row) {
            const td = document.createElement("td");
            td.textContent = datum;
            tr.appendChild(td);
        }
        tbody.appendChild(tr);
    }
    table.appendChild(tbody);
    fragment.appendChild(table);

    return fragment;
}

async function main() {
    var instance: any;

    function getMemoryArray(): Uint8Array {
        return new Uint8Array(instance.exports.memory.buffer);
    }

    function getSlice(ptr: number, len: number): Uint8Array {
        return getMemoryArray().subarray(ptr, ptr + len);
    }

    function makeSliceGetter(name: string): () => Uint8Array {
        return (): Uint8Array => {
            const ptr = instance.exports[`${name}_ptr`]();
            const len = instance.exports[`${name}_len`]();
            return getSlice(ptr, len);
        };
    }

    const getQueryBufferSlice = makeSliceGetter("query_buffer");
    const getQueryResultsSlice = makeSliceGetter("query_results");
    const getErrorResultsSlice = makeSliceGetter("error_results");
    const getDownloadResultsSlice = makeSliceGetter("download_results");
    const getDBUploadSlice = makeSliceGetter("db_upload");

    function readString(slice: Uint8Array): string {
        return decoder.decode(slice);
    }

    function writeQueryString(str: string): void {
        let encoded_str = encoder.encode(str);
        if (encoded_str.length >= (8 * 1024) - 1) {
            throw new Error("Query string longer than 8KiB maximum");
        }
        getQueryBufferSlice().set(encoded_str);
        getQueryBufferSlice().fill(0, encoded_str.length, encoded_str.length + 3);
    }

    function getResultsString(): string {
        return readString(getQueryResultsSlice());
    }

    function getErrorString(): string {
        return readString(getErrorResultsSlice());
    }

    const importObject = {
        "env": {
            get_date_interop(): number {
                return Date.now();
            },
            log_string_interop(src: number, len: number, level: number): void {
                const str = readString(getSlice(src, len));
                if (level == 0) {
                    console.error(str);
                } else if (level == 1) {
                    console.warn(str);
                } else if (level == 2) {
                    console.info(str);
                } else if (level == 3) {
                    console.debug(str);
                } else {
                    console.log(str);
                }
            },
            get_random_values_interop(dest: number, bytes: number): void {
                crypto.getRandomValues(getSlice(dest, bytes));
            },
        },
    };

    const results = await WebAssembly.instantiateStreaming(fetch(sqlite_api), importObject);

    instance = results.instance;

    console.log(instance.exports.setup_sqlite());

    function query(str: string): QueryResult {
        writeQueryString(str);
        const test_res_code = instance.exports.test_sqlite();
        if (test_res_code != 0) {
            return { status: "failure", test_res_code, error_message: getErrorString() };
        }
        const result = JSON.parse(getResultsString());
        instance.exports.test_sqlite_cleanup();
        return result;
    }

    const run_query_button = document.getElementById('runQueryButton');
    if (run_query_button === null) {
        throw new Error("Could get run query button element");
    }

    const create_download_button = document.getElementById('createDownloadButton');
    if (create_download_button === null) {
        throw new Error("Could get create download button element");
    }

    const results_table_container = document.getElementById('resultsTableContainer');
    if (results_table_container === null) {
        throw new Error("Could not get results table container.");
    }

    const error_container = document.getElementById('errorContainer');
    if (error_container === null) {
        throw new Error("Could not get error container.");
    }

    const editor_container = document.getElementById('editorContainer');
    if (editor_container === null) {
        throw new Error("Could get editor container element");
    }

    const editor = monaco.editor.create(editor_container, {
        value: "select * from sqlite_schema;",
        language: "sql",
    });

    const query_and_show_results = (str: string): void => {
        let result: QueryResult = query(str);
        console.log({ result })

        if (result.status == "success") {
            results_table_container.textContent = "";
            results_table_container.appendChild(build_results_table(result));
            error_container.textContent = ""
        } else {
            results_table_container.textContent = "";
            error_container.textContent = ""
            error_container.innerText = "Error: " + result.error_message;
        }
    }

    function createDownload() {
        const loading_text = <HTMLSpanElement>document.getElementById('loadingDownloadText');
        const download_anchor = <HTMLAnchorElement>document.getElementById('downloadAnchor');

        if (download_anchor.href != "") {
            URL.revokeObjectURL(download_anchor.href);
        }

        download_anchor.href = "";
        loading_text.hidden = false;

        const backup_res_code = instance.exports.save_db();
        if (backup_res_code != 0) {
            throw new Error(`Backup failed with code: ${backup_res_code}`);
        }

        const arr = getDownloadResultsSlice();
        const blob = new Blob([arr]);
        const url = URL.createObjectURL(blob);


        download_anchor.href = url;
        loading_text.hidden = true;
    }

    async function loadDatabase() {
        const database_load_select = <HTMLSelectElement>document.getElementById("databaseLoadSelect");
        const database_load_file_input = <HTMLInputElement>document.getElementById("databaseUploadFileInput");
        if (database_load_select.value == "fileupload") {
            if (database_load_file_input.files == null) {
                throw new Error("files was null");
            }
            const file = database_load_file_input.files[0];
            console.log(file);
            const upload_bytes = new Uint8Array(await file.arrayBuffer());
            instance.exports.allocate_db_upload(file.size);
            getDBUploadSlice().set(upload_bytes);
            console.log("db load result: ", instance.exports.load_db_upload());
        } else if (database_load_select.value == "w3schoolsdemodb") {
            instance.exports.load_test_db();
        } else {
            throw new Error("unknown database load option.");
        }
    }
    (<HTMLButtonElement>document.getElementById("loadDatabaseButton")).addEventListener("click", loadDatabase);


    Object.assign(window, { instance, createDownload });

    const run_query = () => {
        query_and_show_results(editor.getValue());
    }

    run_query_button.addEventListener('click', run_query);
    create_download_button.addEventListener('click', createDownload);

    editor.addAction({
        id: "runQuery",
        label: "Run Query",
        keybindings: [monaco.KeyMod.Alt | monaco.KeyCode.KeyR],
        run: run_query,
    })

    window.addEventListener("resize", () => {
        editor.layout();
    });

    // instance.exports.cleanup_sqlite();
}

document.addEventListener("DOMContentLoaded", main);