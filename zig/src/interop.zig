// Function to fill a buffer with random bytes
extern fn get_random_values_interop([*]u8, usize) void;
pub fn get_random_values(buff: []u8) void {
    get_random_values_interop(buff.ptr, buff.len);
}

// Function for logging messages
extern fn log_string_interop([*]const u8, usize, i32) void;
pub fn log_string(str: []const u8, level: i32) void {
    log_string_interop(str.ptr, str.len, level);
}

// Take a slice and export two functions giving it's length and address.
pub fn export_slice(comptime name: []const u8, comptime slice: *?[]const u8) type {
    return struct {
        fn ptr() callconv(.C) ?[*]const u8 {
            return (slice.* orelse return null).ptr;
        }

        fn len() callconv(.C) usize {
            return (slice.* orelse return 0).len;
        }

        comptime {
            @export(ptr, .{ .name = name ++ "_ptr" });
            @export(len, .{ .name = name ++ "_len" });
        }
    };
}

extern fn get_date_interop() f64;

pub fn getJulianDate() f64 {
    const epoch_ms = get_date_interop();
    return epoch_ms / 86_400_000.0 + 2_440_587.5;
}
