const std = @import("std");
const mem = std.mem;
const expect = std.testing.expect;
const expectEqual = std.testing.expectEqual;
const interop = @import("interop.zig");

export fn __stack_chk_fail() void {}

export fn strcmp(a: [*:0]const u8, b: [*:0]const u8) i8 {
    return switch (std.mem.orderZ(u8, a, b)) {
        .lt => -1,
        .eq => 0,
        .gt => 1,
    };
}

export fn memcpy(dest: [*]u8, src: [*]const u8, n: usize) [*]u8 {
    mem.copy(u8, dest[0..n], src[0..n]);
    return dest;
}

export fn strlen(s: [*:0]const u8) usize {
    return mem.len(s);
}

export fn memset(dest: [*]u8, val: u8, n: usize) [*]u8 {
    @memset(dest[0..n], val);
    return dest;
}

export fn memcmp(a: [*]const u8, b: [*]const u8, n: usize) i32 {
    const slice_a = a[0..n];
    const slice_b = b[0..n];
    const pos = mem.indexOfDiff(u8, slice_a, slice_b) orelse return 0;
    return @as(i32, slice_a[pos]) - @as(i32, slice_b[pos]);
}

export fn memmove(dest: [*]u8, src: [*]const u8, n: usize) [*]u8 {
    if (@intFromPtr(dest) > @intFromPtr(src)) {
        mem.copyBackwards(u8, dest[0..n], src[0..n]);
    } else {
        mem.copy(u8, dest[0..n], src[0..n]);
    }
    return dest;
}

export fn strcspn(s: [*:0]const u8, c: [*:0]const u8) usize {
    const slice_s = mem.span(s);
    const slice_c = mem.span(c);
    return mem.indexOfAny(u8, slice_s, slice_c) orelse slice_s.len;
}

test "libc strcspn" {
    try expectEqual(@as(usize, 0), strcspn("", ""));
    try expectEqual(@as(usize, 0), strcspn("ABCDEFGHIJK", "ABC"));
    try expectEqual(@as(usize, 3), strcspn("DEFBCA", "ABC"));
    try expectEqual(@as(usize, 3), strcspn("DEF", "ABC"));
}

export fn strncmp(a: [*:0]const u8, b: [*:0]const u8, n: usize) i32 {
    var i: usize = 0;
    while (i < n) : (i += 1) {
        const val_a = a[i];
        const val_b = b[i];

        if (val_a == val_b) {
            if (val_a == 0) {
                return 0;
            } else {
                continue;
            }
        } else {
            return @as(i32, val_a) - @as(i32, val_b);
        }
    }
    return 0;
}

export fn strrchr(s: [*:0]const u8, c: u8) ?*const u8 {
    const slice = s[0..mem.len(s)];
    const index = mem.lastIndexOfScalar(u8, slice, c) orelse return null;
    return &s[index];
}

// Deliberately trapped
export fn localtime(_: *const anyopaque) *const anyopaque {
    unreachable;
}

// Don't need the memory allocation functions, we customize memory allocation
// in sqlite.
export fn malloc(_: usize) [*]u8 {
    unreachable;
}

export fn free(_: [*]u8) void {
    unreachable;
}

export fn realloc(_: [*]u8, _: usize) [*]u8 {
    unreachable;
}
