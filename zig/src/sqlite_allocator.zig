const std = @import("std");
const mem = std.mem;
const sqlite = @import("sqlite.zig");

pub const SqliteWasmAllocator = struct {
    backing_allocator: mem.Allocator,
    allocation_sizes: std.AutoHashMap(usize, usize),

    var backing_allocator: mem.Allocator = undefined;

    var global_state: SqliteWasmAllocator = undefined;

    fn getSize(self: *SqliteWasmAllocator, ptr: [*]u8) ?usize {
        return self.allocation_sizes.get(@intFromPtr(ptr));
    }

    fn getSlice(self: *SqliteWasmAllocator, ptr: [*]u8) ?[]u8 {
        const size = self.getSize(ptr) orelse return null;
        return ptr[0..size];
    }

    pub fn init(alloc: mem.Allocator) void {
        backing_allocator = alloc;
    }

    fn xInit(_: ?*anyopaque) callconv(.C) i32 {
        global_state = SqliteWasmAllocator{
            .backing_allocator = backing_allocator,
            .allocation_sizes = std.AutoHashMap(usize, usize).init(backing_allocator),
        };
        return sqlite.SQLITE_OK;
    }

    fn xShutdown(_: ?*anyopaque) callconv(.C) void {
        global_state.allocation_sizes.deinit();
    }

    fn xMalloc(pre_size: i32) callconv(.C) ?*anyopaque {
        const size = @as(usize, @intCast(pre_size));
        const allocation = global_state.backing_allocator.alloc(u8, size) catch {
            // Allocation failed. Return null.
            return null;
        };
        global_state.allocation_sizes.put(@intFromPtr(allocation.ptr), size) catch {
            // Allocation succeded, but the hashtable failed. Free the new
            // allocation and return null.
            global_state.backing_allocator.free(allocation);
            return null;
        };
        return allocation.ptr;
    }

    fn xFree(pre_ptr: ?*anyopaque) callconv(.C) void {
        const ptr = @as([*]u8, @ptrCast(pre_ptr orelse return));
        const size_entry = global_state.allocation_sizes.fetchRemove(@intFromPtr(ptr)) orelse return;
        const slice = ptr[0..size_entry.value];
        global_state.backing_allocator.free(slice);
    }

    fn xRealloc(pre_ptr: ?*anyopaque, pre_new_size: i32) callconv(.C) ?*anyopaque {
        const ptr = @as([*]u8, @ptrCast(pre_ptr orelse return null));
        const new_size = @as(usize, @intCast(pre_new_size));
        const old_slice = global_state.getSlice(ptr) orelse return null;
        const new_allocation = global_state.backing_allocator.realloc(old_slice, new_size) catch {
            return null;
        };

        const new_ptr = new_allocation.ptr;
        if (@intFromPtr(ptr) == @intFromPtr(new_ptr)) {
            // We can assume capacity here because we know the allocation is already
            // in the table.
            global_state.allocation_sizes.putAssumeCapacity(@intFromPtr(ptr), new_size);
        } else {
            _ = global_state.allocation_sizes.remove(@intFromPtr(ptr));
            // This may come back to bite me later.
            global_state.allocation_sizes.put(@intFromPtr(new_ptr), new_size) catch unreachable;
        }
        return new_ptr;
    }

    fn xSize(pre_ptr: ?*anyopaque) callconv(.C) i32 {
        const ptr = @as([*]u8, @ptrCast(pre_ptr));
        // If this happens we should change it to an error handling of some sort.
        const size = global_state.getSize(ptr) orelse @panic("Allocation does not exist");
        return @as(i32, @intCast(size));
    }

    fn xRoundup(size: i32) callconv(.C) i32 {
        // No rounding is needed. The GPA is able to handle allocations of any
        // size well enough.
        return size;
    }

    pub const mem_methods = sqlite.sqlite3_mem_methods{
        .xMalloc = xMalloc,
        .xFree = xFree,
        .xRealloc = xRealloc,
        .xSize = xSize,
        .xRoundup = xRoundup,
        .xInit = xInit,
        .xShutdown = xShutdown,
        .pAppData = null,
    };
};
