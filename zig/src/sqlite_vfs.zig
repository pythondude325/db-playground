const std = @import("std");
const sqlite = @import("sqlite.zig");
const interop = @import("interop.zig");

// Sqlite VFS from https://www.sqlite.org/c3ref/vfs.html

const test_db_file = @embedFile("w3_db.sqlite");

fn xRandomness(_: ?*sqlite.sqlite3_vfs, nByte: i32, zOut: ?[*]u8) callconv(.C) i32 {
    const output = zOut orelse return 0;
    const size = @as(usize, @intCast(nByte));
    const out_slice: []u8 = output[0..size];
    interop.get_random_values(out_slice);
    return nByte;
}

fn xCurrentTime(_: ?*sqlite.sqlite3_vfs, dest: ?*f64) callconv(.C) i32 {
    const dest_ptr = dest orelse return sqlite.SQLITE_ERROR;
    const t = interop.getJulianDate();
    dest_ptr.* = t;
    return sqlite.SQLITE_OK;
}

pub const FileBacking = union(enum) {
    rw: struct {
        array: std.ArrayList(u8),
        delete_on_close: bool,
    },
    ro: []const u8,

    pub fn slice(self: FileBacking) []const u8 {
        return switch (self) {
            FileBacking.rw => |a| a.array.items,
            FileBacking.ro => |s| s,
        };
    }

    fn close(self: *FileBacking) void {
        switch (self.*) {
            .rw => |*rw| {
                if (rw.*.delete_on_close) {
                    rw.*.array.deinit();
                    Filesystem.allocator.destroy(self);
                }
            },
            else => {},
        }
    }
};

pub const Filesystem = struct {
    pub var allocator: std.mem.Allocator = undefined;
    pub var inFile: FileBacking = FileBacking{ .ro = test_db_file };
    pub var outFile: FileBacking = undefined;

    pub fn load_infile(s: []const u8) void {
        inFile = FileBacking{ .ro = s };
    }

    pub fn load_infile_test_db() void {
        load_infile(test_db_file);
    }

    pub fn init(alloc: std.mem.Allocator) void {
        allocator = alloc;
        outFile = FileBacking{ .rw = .{ .array = std.ArrayList(u8).init(allocator), .delete_on_close = false } };
    }

    pub fn create_rw() error{OutOfMemory}!*FileBacking {
        const buffer = try allocator.create(FileBacking);
        buffer.* = FileBacking{ .rw = .{ .array = std.ArrayList(u8).init(allocator), .delete_on_close = true } };
        return buffer;
    }
};

const VFSFile = extern struct {
    fn fromFile(self: ?*sqlite.sqlite3_file) error{NullError}!*VFSFile {
        if (self) |s| {
            return @ptrCast(s);
        } else {
            return error.NullError;
        }
    }

    fn xSectorSize(_: ?*sqlite.sqlite3_file) callconv(.C) i32 {
        return 1;
    }

    fn xDeviceCharacteristics(selfFile: ?*sqlite.sqlite3_file) callconv(.C) i32 {
        const self = VFSFile.fromFile(selfFile orelse return sqlite.SQLITE_ERROR) catch return sqlite.SQLITE_ERROR;
        const immutable = if (self.inner().*.buffer.* == FileBacking.ro) sqlite.SQLITE_IOCAP_IMMUTABLE else 0;
        return sqlite.SQLITE_IOCAP_ATOMIC | immutable;
    }

    fn xSync(_: ?*sqlite.sqlite3_file, _: i32) callconv(.C) i32 {
        return sqlite.SQLITE_OK; // Our file is always synced because we have syncronous writes
    }

    // Do nothing to lock/unlock
    fn xLock(_: ?*sqlite.sqlite3_file, _: i32) callconv(.C) i32 {
        std.log.warn("xLock or xUnlock called.", .{});
        return sqlite.SQLITE_OK;
    }

    fn xCheckReservedLock(_: ?*sqlite.sqlite3_file, _: ?*i32) callconv(.C) i32 {
        std.log.err("CheckReservedLock called.", .{});
        return sqlite.SQLITE_ERROR;
    }

    fn xFileControl(_: ?*sqlite.sqlite3_file, op: i32, pArg: ?*anyopaque) callconv(.C) i32 {
        _ = pArg;
        switch (op) {
            5 => { // Size Hint
                std.log.debug("xFileControl called with op 5 SQLITE_FCNTL_SIZE_HINT.", .{});
                // TODO: implement reserving extra capacity in the array based on this size hint.
                return sqlite.SQLITE_OK;
            },
            15 => {
                std.log.debug("xFileControl called with op 15 SQLITE_FCNTL_BUSYHANDLER.", .{});
                return sqlite.SQLITE_OK;
            },
            21 => { // Sync
                // We don't need to do anything. Sync is a no-op for us.
                std.log.debug("xFileControl called with op 21 SQLITE_FCNTL_SYNC.", .{});
                return sqlite.SQLITE_OK;
            },
            30 => {
                std.log.debug("xFileControl called with op 30 SQLITE_FCNTL_PDB.", .{});
                return sqlite.SQLITE_OK;
            },
            else => {
                std.log.warn("xFileControl called with an unimplemented op {d}", .{op});
                return sqlite.SQLITE_OK;
            },
        }
    }

    //

    fn xFileSize(selfFile: ?*sqlite.sqlite3_file, pSize: ?*i64) callconv(.C) i32 {
        std.log.debug("xFileSize called.", .{});
        const size = pSize orelse return sqlite.SQLITE_ERROR;
        const self = VFSFile.fromFile(selfFile) catch return sqlite.SQLITE_ERROR;
        const file_size = @as(i64, self.inner().*.buffer.slice().len);
        std.log.debug("File size was {d}.", .{file_size});
        size.* = file_size;
        return sqlite.SQLITE_OK;
    }

    fn xRead(selfFile: ?*sqlite.sqlite3_file, destPtr: ?*anyopaque, amtI32: isize, offsetI64: i64) callconv(.C) i32 {
        const self = VFSFile.fromFile(selfFile) catch return sqlite.SQLITE_ERROR;
        std.log.debug("read occurred on file '{s}'. amt:{d} off:{d}", .{ self.filename(), amtI32, offsetI64 });

        const amt: usize = std.math.cast(usize, amtI32) orelse return sqlite.SQLITE_ERROR;
        const offset: isize = std.math.cast(isize, offsetI64) orelse return sqlite.SQLITE_ERROR;
        const dest: []u8 = @as([*]u8, @ptrCast(destPtr orelse return sqlite.SQLITE_ERROR))[0..@as(usize, @intCast(amt))];

        // const start: isize = @as(isize, @intCast(self.inner().*.pos)) + offset;
        const start: isize = offset;
        const end: isize = start + amtI32;

        const data = self.inner().*.buffer.*.slice();

        const ustart = std.math.cast(usize, start) orelse return sqlite.SQLITE_ERROR;
        const uend = std.math.cast(usize, end) orelse return sqlite.SQLITE_ERROR;

        if (end > data.len) {
            // Short read
            const readData = data[ustart..];
            @memcpy(dest[0..readData.len], readData);
            @memset(dest[readData.len..], 0);

            std.log.debug("short read succeded.", .{});
            return sqlite.SQLITE_IOERR_SHORT_READ;
        } else {
            @memcpy(dest, data[ustart..uend]);
            std.log.debug("read succeded.", .{});
            return sqlite.SQLITE_OK;
        }
    }

    fn xWrite(selfFile: ?*sqlite.sqlite3_file, srcPtr: ?*const anyopaque, amt: i32, offset: i64) callconv(.C) i32 {
        const self = VFSFile.fromFile(selfFile) catch return sqlite.SQLITE_ERROR;
        std.log.debug("xWrite called on file '{s}'. amt:{d} off:{d}", .{ self.filename(), amt, offset });

        switch (self.inner().*.buffer.*) {
            .rw => |*rw| {
                const src: []const u8 = @as([*]const u8, @ptrCast(srcPtr orelse return sqlite.SQLITE_ERROR))[0..@as(usize, @intCast(amt))];

                const start: isize = @intCast(offset);
                const end: isize = start + amt;

                const ustart: usize = std.math.cast(usize, start) orelse return sqlite.SQLITE_ERROR;
                const uend: usize = std.math.cast(usize, end) orelse return sqlite.SQLITE_ERROR;

                if (uend > rw.array.items.len) {
                    rw.array.resize(uend) catch return sqlite.SQLITE_IOERR_NOMEM;
                }

                @memcpy(rw.array.items[ustart..uend], src);

                return sqlite.SQLITE_OK;
            },
            .ro => {
                return sqlite.SQLITE_IOERR_WRITE;
            },
        }
    }

    fn xTruncate(_: ?*sqlite.sqlite3_file, _: i64) callconv(.C) i32 {
        std.log.err("xTruncate called.", .{});
        return sqlite.SQLITE_ERROR;
    }

    fn xClose(selfFile: ?*sqlite.sqlite3_file) callconv(.C) i32 {
        const self = VFSFile.fromFile(selfFile) catch return sqlite.SQLITE_ERROR;
        std.log.debug("xClose called on file '{s}'.", .{self.filename()});
        self.inner().*.buffer.close();
        return sqlite.SQLITE_OK;
    }

    var vfs_file_io_methods = sqlite.sqlite3_io_methods{
        .iVersion = 1,
        .xClose = xClose,
        .xRead = xRead,
        .xWrite = xWrite,
        .xTruncate = xTruncate,
        .xSync = xSync,
        .xFileSize = xFileSize,
        .xLock = xLock,
        .xUnlock = xLock,
        .xCheckReservedLock = xCheckReservedLock,
        .xFileControl = xFileControl,
        .xSectorSize = xSectorSize,
        .xDeviceCharacteristics = xDeviceCharacteristics,
        // Unimplemented
        .xShmMap = null,
        .xShmLock = null,
        .xShmBarrier = null,
        .xShmUnmap = null,
        .xFetch = null,
        .xUnfetch = null,
    };

    pMethods: *sqlite.sqlite3_io_methods,
    inner_bytes: [@sizeOf(Inner)]u8 align(@alignOf(Inner)),

    const Inner = struct {
        buffer: *FileBacking,
        filename: []const u8,
    };

    fn inner(self: *VFSFile) *Inner {
        return std.mem.bytesAsValue(Inner, &self.inner_bytes);
    }

    fn filename(self: *VFSFile) []const u8 {
        return self.inner().*.filename;
    }

    fn init(self: *VFSFile, buffer: *FileBacking, filenameStr: []const u8) void {
        self.*.pMethods = &VFSFile.vfs_file_io_methods;
        self.inner().*.buffer = buffer;
        self.inner().*.filename = filenameStr;
    }
};

fn xOpen(_: ?*sqlite.sqlite3_vfs, zName: ?[*:0]const u8, file: ?*sqlite.sqlite3_file, flags: i32, pOutFlags: ?*i32) callconv(.C) i32 {
    const name = std.mem.span(zName orelse return sqlite.SQLITE_ERROR);
    std.log.debug("opening file {s}", .{name});

    const f = VFSFile.fromFile(file) catch return sqlite.SQLITE_IOERR;
    if (std.mem.eql(u8, name, "in")) {
        f.init(&Filesystem.inFile, "in");
        if (pOutFlags) |outFlags| {
            outFlags.* |= sqlite.SQLITE_OPEN_READONLY;
        }
        std.log.debug("xOpen finished opening 'in'.", .{});
        return sqlite.SQLITE_OK;
    } else if (std.mem.eql(u8, name, "out")) {
        if (flags & sqlite.SQLITE_OPEN_CREATE != 0) {
            switch (Filesystem.outFile) {
                .rw => |*rw| {
                    // Clearing the arraylist should never produce an error.
                    rw.*.array.resize(0) catch unreachable;
                },
                .ro => unreachable,
            }
        }
        f.init(&Filesystem.outFile, "out");
        std.log.debug("xOpen finished opening 'out'.", .{});
        return sqlite.SQLITE_OK;
    } else {
        const rw = flags & sqlite.SQLITE_OPEN_READWRITE != 0;
        const delete_on_close = flags & sqlite.SQLITE_OPEN_DELETEONCLOSE != 0;

        f.init(Filesystem.create_rw() catch return sqlite.SQLITE_NOMEM, name);
        std.log.warn("xOpen opening '{s}' with rw({?}) and doc({?}) opening as RW+DOC.", .{ name, rw, delete_on_close });
        return sqlite.SQLITE_OK;
    }
}

fn xFullPathname(_: ?*sqlite.sqlite3_vfs, zName: ?[*:0]const u8, nOut: isize, zOut: ?[*]u8) callconv(.C) i32 {
    const out: []u8 = (zOut orelse return sqlite.SQLITE_ERROR)[0..@as(usize, @intCast(nOut))];
    const in = std.mem.span(zName orelse return sqlite.SQLITE_ERROR);
    std.mem.copyForwards(u8, out, in);
    out[in.len] = 0;
    return sqlite.SQLITE_OK;
}

pub var vfs = sqlite.sqlite3_vfs{
    .iVersion = 1,
    .szOsFile = @sizeOf(VFSFile),
    .mxPathname = 256,
    .pNext = null,
    .zName = "wasm_vfs",
    .pAppData = null,

    .xOpen = xOpen,
    .xDelete = null,
    .xAccess = xAccess,
    .xFullPathname = xFullPathname,
    .xDlOpen = null,
    .xDlError = null,
    .xDlSym = null,
    .xDlClose = null,

    .xRandomness = xRandomness,
    .xSleep = xSleep,
    .xCurrentTime = xCurrentTime,
    .xGetLastError = xGetLastError,

    .xCurrentTimeInt64 = xCurrentTimeInt64,

    .xSetSystemCall = null,
    .xGetSystemCall = null,
    .xNextSystemCall = null,
};

fn xAccess(_: ?*sqlite.sqlite3_vfs, zName: ?[*:0]const u8, flags: i32, pOut: ?*i32) callconv(.C) i32 {
    const out: *i32 = pOut orelse return sqlite.SQLITE_ERROR;

    var filename: []const u8 = "null";
    if (zName) |name| {
        filename = std.mem.span(name);
    }
    switch (flags) {
        sqlite.SQLITE_ACCESS_EXISTS => {
            std.log.debug("xAccess checking if '{s}' exists.", .{filename});
            if (std.mem.eql(u8, filename, "in") or std.mem.eql(u8, filename, "out")) {
                out.* = 1;
            } else {
                out.* = 0;
            }
            return sqlite.SQLITE_OK;
        },
        sqlite.SQLITE_ACCESS_READWRITE => {
            std.log.err("xAccess checking if '{s}' is RW.", .{filename});
            return sqlite.SQLITE_ERROR;
        },
        else => {
            std.log.err("xAccess called on '{s}' with unknown flags {d}.", .{ filename, flags });
            return sqlite.SQLITE_ERROR;
        },
    }
}

fn xGetLastError(_: ?*sqlite.sqlite3_vfs, _: i32, _: ?[*:0]u8) callconv(.C) i32 {
    std.log.err("xGetLastError called.", .{});
    return sqlite.SQLITE_ERROR;
}

fn xSleep(_: ?*sqlite.sqlite3_vfs, _: i32) callconv(.C) i32 {
    std.log.err("xSleep called.", .{});
    return sqlite.SQLITE_ERROR;
}

fn xCurrentTimeInt64(_: ?*sqlite.sqlite3_vfs, _: ?*i64) callconv(.C) i32 {
    std.log.err("xCurrentTimeInt64 called.", .{});
    return sqlite.SQLITE_ERROR;
}
