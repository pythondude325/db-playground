const std = @import("std");

usingnamespace @import("libc.zig");
const sqlite = @import("sqlite.zig");
const custom_vfs = @import("sqlite_vfs.zig");
const custom_allocator = @import("sqlite_allocator.zig");
const interop = @import("interop.zig");

// Interop IO slices
const query_buffer_length = 8 * 1024;
pub var query_buffer = [_:0]u8{0} ** query_buffer_length;
pub var query_buffer_slice: ?[]u8 = query_buffer[0..];

pub var query_results: ?[]u8 = null;
pub var error_results: ?[]const u8 = null;
pub var download_results: ?[]const u8 = null;
pub var db_upload: ?[]u8 = null;

comptime {
    // Create export functions for the io slices
    _ = interop.export_slice("query_buffer", &query_buffer_slice);
    _ = interop.export_slice("query_results", &query_results);
    _ = interop.export_slice("error_results", &error_results);
    _ = interop.export_slice("download_results", &download_results);
    _ = interop.export_slice("db_upload", &db_upload);
}

var gpa: std.heap.GeneralPurposeAllocator(.{}) = undefined;

export fn sqlite3_os_init() i32 {
    const res = sqlite.sqlite3_vfs_register(&custom_vfs.vfs, 1);

    if (res != sqlite.SQLITE_OK) {
        return res;
    }

    return sqlite.SQLITE_OK;
}

export fn sqlite3_os_end() i32 {
    const res = sqlite.sqlite3_vfs_unregister(&custom_vfs.vfs);

    if (res != sqlite.SQLITE_OK) {
        return res;
    }

    return sqlite.SQLITE_OK;
}

fn backup(dest: *sqlite.sqlite3, src: *sqlite.sqlite3) i32 {
    if (sqlite.sqlite3_backup_init(dest, "main", src, "main")) |backup_obj| {
        while (true) {
            switch (sqlite.sqlite3_backup_step(backup_obj, -1)) {
                sqlite.SQLITE_OK => {},
                sqlite.SQLITE_DONE => {
                    break;
                },
                else => |errcode| {
                    return errcode;
                },
            }
        }
        return sqlite.sqlite3_backup_finish(backup_obj);
    } else {
        return sqlite.SQLITE_ERROR;
    }
}

pub export fn load_test_db() i32 {
    custom_vfs.Filesystem.load_infile_test_db();

    var test_db: ?*sqlite.sqlite3 = null;
    const db_open_res = sqlite.sqlite3_open_v2("in", &test_db, sqlite.SQLITE_OPEN_READONLY, null);
    if (db_open_res != sqlite.SQLITE_OK) {
        return db_open_res;
    }

    const backup_res = backup(db orelse return sqlite.SQLITE_ERROR, test_db orelse return sqlite.SQLITE_ERROR);
    if (backup_res != sqlite.SQLITE_OK) {
        return backup_res;
    }

    return sqlite.sqlite3_close(test_db);
}

pub export fn allocate_db_upload(size: usize) callconv(.C) i32 {
    if (db_upload) |s| {
        db_upload = gpa.allocator().realloc(s, size) catch return sqlite.SQLITE_NOMEM;
    } else {
        db_upload = gpa.allocator().alloc(u8, size) catch return sqlite.SQLITE_NOMEM;
    }
    return sqlite.SQLITE_OK;
}

fn free_db_upload() void {
    if (db_upload) |s| {
        gpa.allocator().free(s);
    }
}

pub export fn load_db_upload() i32 {
    custom_vfs.Filesystem.load_infile(db_upload orelse return sqlite.SQLITE_ERROR);

    var backup_db: ?*sqlite.sqlite3 = null;
    const db_open_res = sqlite.sqlite3_open_v2("in", &backup_db, sqlite.SQLITE_OPEN_READONLY, null);
    if (db_open_res != sqlite.SQLITE_OK) {
        std.log.err("Load db upload open failed.", .{});
        return db_open_res;
    }

    const backup_res = backup(db orelse return sqlite.SQLITE_ERROR, backup_db orelse return sqlite.SQLITE_ERROR);
    if (backup_res != sqlite.SQLITE_OK) {
        std.log.err("Load db upload backup failed.", .{});
        return backup_res;
    }

    const close_res = sqlite.sqlite3_close(backup_db);
    if (close_res != sqlite.SQLITE_OK) {
        return close_res;
    }

    free_db_upload();

    return sqlite.SQLITE_OK;
}

pub export fn save_db() i32 {
    std.log.info("Creating database backup.", .{});

    var out_db: ?*sqlite.sqlite3 = null;
    const db_open_res = sqlite.sqlite3_open_v2("out", &out_db, sqlite.SQLITE_OPEN_CREATE | sqlite.SQLITE_OPEN_READWRITE, null);
    if (db_open_res != sqlite.SQLITE_OK) {
        return db_open_res;
    }

    const backup_res = backup(out_db orelse return sqlite.SQLITE_ERROR, db orelse return sqlite.SQLITE_ERROR);
    if (backup_res != sqlite.SQLITE_OK) {
        return backup_res;
    }

    const close_res = sqlite.sqlite3_close(out_db);
    if (close_res != sqlite.SQLITE_OK) {
        return close_res;
    }

    download_results = custom_vfs.Filesystem.outFile.slice();

    std.log.info("Database backup complete.", .{});

    return sqlite.SQLITE_OK;
}

var db: ?*sqlite.sqlite3 = null;

pub export fn setup_sqlite() i32 {
    gpa = std.heap.GeneralPurposeAllocator(.{}){};
    custom_allocator.SqliteWasmAllocator.init(gpa.allocator());
    custom_vfs.Filesystem.init(gpa.allocator());

    // sqlite.SQLITE_CONIFG_MALLOC is not defined for some reason
    const mem_config_res = sqlite.sqlite3_config(4, &custom_allocator.SqliteWasmAllocator.mem_methods);
    if (mem_config_res != sqlite.SQLITE_OK) {
        return mem_config_res;
    }

    // sqlite.SQLITE_CONIFG_LOG
    const log_config_res = sqlite.sqlite3_config(16, sqlite_error_logger, @as(?*anyopaque, null));
    if (log_config_res != sqlite.SQLITE_OK) {
        return log_config_res;
    }

    const res = sqlite.sqlite3_initialize();
    if (res != sqlite.SQLITE_OK) {
        return res;
    }

    std.log.debug("Opening Sqlite DB", .{});
    const db_open_res = sqlite.sqlite3_open(":memory:", &db);
    std.log.debug("Opened file!", .{});
    // const db_open_res = sqlite.sqlite3_open_v2("WasmDB", &db, sqlite.SQLITE_OPEN_READWRITE | sqlite.SQLITE_OPEN_MEMORY, "wasm_vfs");
    if (db_open_res != sqlite.SQLITE_OK) {
        return db_open_res;
    }

    const backup_res = load_test_db();
    if (backup_res != sqlite.SQLITE_OK) {
        return backup_res;
    }

    return sqlite.SQLITE_OK;
}

pub export fn cleanup_sqlite() i32 {
    const db_close_res = sqlite.sqlite3_close(db);
    if (db_close_res != sqlite.SQLITE_OK) {
        return db_close_res;
    }

    const shutdown_res = sqlite.sqlite3_shutdown();
    if (shutdown_res != sqlite.SQLITE_OK) {
        return shutdown_res;
    }

    return sqlite.SQLITE_OK;
}

pub export fn sqlite_explain_error() i32 {
    const err_msg: [*:0]const u8 = sqlite.sqlite3_errmsg(db) orelse return sqlite.SQLITE_ERROR;
    std.log.err("sqlite error: {s}", .{std.mem.span(err_msg)});
    return sqlite.SQLITE_OK;
}

pub export fn test_sqlite() i32 {
    var prepare_stmt: ?*sqlite.sqlite3_stmt = null;
    const prepare_rc = sqlite.sqlite3_prepare_v2(db, &query_buffer, query_buffer_length, &prepare_stmt, null);
    if (prepare_rc != sqlite.SQLITE_OK) {
        std.log.err("Error while preparing query statement", .{});

        const err_msg: [*:0]const u8 = sqlite.sqlite3_errmsg(db) orelse return sqlite.SQLITE_ERROR;
        std.log.err("sqlite error: {s}", .{std.mem.span(err_msg)});
        error_results = std.mem.span(err_msg);

        return prepare_rc;
    }
    const stmt: *sqlite.sqlite3_stmt = prepare_stmt orelse return sqlite.SQLITE_ERROR;
    defer _ = sqlite.sqlite3_finalize(stmt); // Free the stmt once we are done with it.

    std.log.debug("prepared stmt", .{});

    const column_count = @as(usize, @intCast(sqlite.sqlite3_column_count(stmt)));
    const column_names = gpa.allocator().alloc([:0]const u8, column_count) catch return sqlite.SQLITE_NOMEM;
    defer gpa.allocator().free(column_names); // Free the list of column names.
    // Fill hte lst of column names
    var column_index: usize = 0;
    while (column_index < column_count) : (column_index += 1) {
        const column_name: [*:0]const u8 = sqlite.sqlite3_column_name(stmt, @as(i32, @intCast(column_index))) orelse return sqlite.SQLITE_ERROR;
        column_names[column_index] = std.mem.span(column_name);
    }

    var rows = std.ArrayList([]const []const u8).init(gpa.allocator());
    defer {
        for (rows.items) |row| {
            gpa.allocator().free(row);
        }
        rows.deinit();
    }

    var step_rc = sqlite.SQLITE_BUSY;
    while (step_rc != sqlite.SQLITE_DONE) : (step_rc = sqlite.sqlite3_step(stmt)) {
        switch (step_rc) {
            sqlite.SQLITE_BUSY => {
                continue;
            },
            sqlite.SQLITE_ROW => {
                const row = gpa.allocator().alloc([]const u8, column_count) catch return sqlite.SQLITE_NOMEM;
                // TODO: should write a special error defer here
                var i: usize = 0;
                while (i < column_count) : (i += 1) {
                    const column_result_null: ?[*:0]const u8 = sqlite.sqlite3_column_text(stmt, @as(i32, @intCast(i)));
                    if (column_result_null) |column_result| {
                        row[i] = gpa.allocator().dupe(u8, std.mem.span(column_result)) catch return sqlite.SQLITE_NOMEM;
                    } else {
                        const res = sqlite.sqlite3_errcode(db);
                        if (res == sqlite.SQLITE_ROW) {
                            row[i] = gpa.allocator().dupe(u8, "(null)") catch return sqlite.SQLITE_NOMEM;
                        } else {
                            std.log.err("Sqlite column get error. {d}", .{res});
                            return res;
                        }
                    }
                }
                rows.append(row) catch return sqlite.SQLITE_NOMEM;
            },
            sqlite.SQLITE_ERROR => {
                std.log.err("sqlite hit error during query step", .{});
                return sqlite.SQLITE_ERROR;
            },
            else => {
                std.log.err("Encountered unknown error condition in statement evaluation.", .{});
                return sqlite.SQLITE_ERROR;
            },
        }
    }

    std.log.debug("got sqlite results", .{});

    var output_buffer = std.ArrayList(u8).init(gpa.allocator());
    errdefer output_buffer.deinit();

    std.json.stringify(.{
        .status = "success",
        .columns = column_names,
        .results = rows.items,
    }, .{}, output_buffer.writer()) catch return sqlite.SQLITE_NOMEM;

    query_results = output_buffer.toOwnedSlice() catch return sqlite.SQLITE_NOMEM;

    return sqlite.SQLITE_OK;
}

pub export fn test_sqlite_cleanup() void {
    @memset(query_buffer[0..query_buffer.len], 0);
    if (query_results) |r| {
        gpa.allocator().free(r);
        query_results = null;
    }
}

fn sqlite_error_logger(_: ?*anyopaque, errCode: i32, errMsg: ?[*:0]const u8) callconv(.C) void {
    const message = if (errMsg) |e| std.mem.span(e) else "(null)";
    std.log.err("SQLite error report: ({d}) {s}", .{ errCode, message });
}

pub const std_options = struct {
    pub const log_level: std.log.Level = .debug;
    // My own logging function built using my logging interop
    pub fn logFn(
        comptime message_level: std.log.Level,
        comptime scope: @Type(.EnumLiteral),
        comptime format: []const u8,
        args: anytype,
    ) void {
        const level_txt = comptime message_level.asText();
        const prefix2 = if (scope == .default) ": " else "(" ++ @tagName(scope) ++ "): ";

        var buffer: [4 * 1024]u8 = undefined;
        var fba = std.heap.FixedBufferAllocator.init(&buffer);

        var list = std.ArrayList(u8).init(fba.allocator());
        defer list.deinit();
        list.writer().print(level_txt ++ prefix2 ++ format ++ "\n", args) catch return;

        nosuspend interop.log_string(list.items, @intFromEnum(message_level));
    }
};
