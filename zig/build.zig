const std = @import("std");

pub fn build(b: *std.Build) !void {
    const optimize = b.standardOptimizeOption(.{});

    const wasm_features = std.Target.wasm.featureSet(&[_]std.Target.wasm.Feature{ std.Target.wasm.Feature.bulk_memory, std.Target.wasm.Feature.simd128, std.Target.wasm.Feature.nontrapping_fptoint });

    const target = .{ .cpu_arch = .wasm32, .os_tag = .freestanding, .cpu_features_add = wasm_features };

    const lib = b.addSharedLibrary(.{
        .name = "sqlite_api",
        .root_source_file = .{ .path = "src/main.zig" },
        .optimize = optimize,
        .target = target,
        .link_libc = true,
    });
    lib.rdynamic = true;
    lib.stack_protector = false;
    lib.strip = false;

    try lib.include_dirs.append(.{ .path = .{ .path = "./sqlite" } });
    lib.addCSourceFile(.{
        .file = .{ .path = "./sqlite/sqlite3.c" },
        .flags = &[_][]const u8{
            "-DSQLITE_DQS=0",
            "-DSQLITE_THREADSAFE=0",
            "-DSQLITE_OMIT_SHARED_CACHE",
            "-DSQLITE_DEFAULT_MEMSTATUS=0",
            "-DSQLITE_LIKE_DOESNT_MATCH_BLOBS",
            "-DSQLITE_MAX_EXPR_DEPTH=0",
            "-DSQLITE_OMIT_DECLTYPE",
            "-DSQLITE_OMIT_LOAD_EXTENSION",

            "-DSQLITE_OMIT_DEPRECATED",
            "-DSQLITE_OMIT_PROGRESS_CALLBACK",
            "-DSQLITE_DEFAULT_WAL_SYNCHRONOUS=1",
            "-DSQLITE_OMIT_AUTOINIT",
            "-DSQLITE_OS_OTHER=1",
        },
    });

    b.installArtifact(lib);

    const install_lib = b.addInstallArtifact(lib, .{});
    // This is needed because parcel is being dumb and doesn't update when the
    // wasm module changes.
    const touch_parcelrc = b.addSystemCommand(&[_][]const u8{
        "/usr/bin/touch",
        "../www/.parcelrc",
    });
    touch_parcelrc.step.dependOn(&install_lib.step);

    const parcel_step = b.step("parcel", "Build and touch parcel");
    parcel_step.dependOn(&install_lib.step);
    parcel_step.dependOn(&touch_parcelrc.step);

    var main_tests = b.addTest(.{
        .root_source_file = .{ .path = "src/main.zig" },
        .link_libc = true,
        .optimize = optimize,
        .target = target,
    });
    try main_tests.include_dirs.append(.{ .path = .{ .path = "./sqlite" } });
    const test_step = b.step("test", "Run library tests");
    test_step.dependOn(&main_tests.step);

    const libc_tests = b.addTest(.{
        .root_source_file = .{ .path = "src/libc.zig" },
        .optimize = optimize,
        .target = target,
    });
    const test_libc_step = b.step("test-libc", "Run libc tests");
    test_libc_step.dependOn(&libc_tests.step);
}
